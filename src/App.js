import React from 'react';
import Header from "./components/Header";
import './App.css';
import { Route, Switch, withRouter } from 'react-router-dom';
import Footer from './components/Footer';
import Home from './pages/Home/Home';
import Login from "./pages/Login";
import Register from "./pages/Register";
import Settings from "./pages/Settings";
import { inject, observer } from 'mobx-react';
import Editor from './pages/Editor';
import Article from './pages/Article/Article';
import Profile from './pages/Profile';

@withRouter
@inject('authStore', 'userStore')
@observer
class App extends React.Component {
  componentDidMount() {
    if(this.props.authStore.token) {
      this.props.userStore.loadUser();
    }
  }
  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/settings" component={Settings} />
          <Route path="/editor/:slug?" component={Editor} />
          <Route path="/article/:slug?" component={Article} />
          <Route path="/profile/:username?" component={Profile} />
          <Route path="/profile/:username?/favorites" component={Profile} />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default App;
