const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
  app.use(
    createProxyMiddleware('/api', {
      target: 'https://conduit.productionready.io',
      changeOrigin: true
    })
  );  
};