import axios from "axios";
import { action, observable, reaction } from "mobx";

class AuthStore {
    @observable inProgress = false;
    @observable token = window.localStorage.getItem('jwt');
    @observable errors;
    @observable loginForm = {
        email: '',
        username: '',
        password: ''
    }
    

    @action setEmail(email) {
        this.loginForm.email = email;
    }

    @action setPassword(password) {
        this.loginForm.password = password;
    }

    @action setUsername(username) {
        this.loginForm.username = username;
    }

    @action reset() {
        this.loginForm.email = '';
        this.loginForm.password = '';
        this.loginForm.username = '';
    }

    @action async login() {
        this.errors = undefined;
        this.inProgress = true;
        return axios.post('/api/users/login', {user: {
            email: this.loginForm.email,
            password: this.loginForm.password
        }}).then((res) => {
            const {user} = res.data;            
            this.token = user.token;            
        }).then(() => {
            this.rootStore.userStore.loadUser();
        }).catch((error) => {
            if(error.response) {
                this.errors = error.response.data.errors;
            }
            throw error;
        }).finally(() => {
            this.inProgress = false;
        });
    }

    @action logout() {
        this.token = undefined;
        this.rootStore.userStore.currentUser = undefined;
        return Promise.resolve();
    }

    @action async regist() {
        this.errors = undefined;
        this.inProgress = true;
        return axios.post('/api/users', {user: {
            email: this.loginForm.email,
            username: this.loginForm.username,
            password: this.loginForm.password
        }}).then((res) => {
            const {user} = res.data;            
            this.token = user.token;                 
        }).catch((error) => {
            console.log(error);
            if(error.response) {
                this.errors = error.response.data.errors.body;
            }
            throw error;
        }).then(() => {
            this.rootStore.userStore.loadUser();       
        }).finally(() => {
            this.inProgress = false;
        });
    }

    constructor(rootStore) {
        this.rootStore = rootStore;

        reaction(
            () => this.token,
            token => {
                if(token) {
                    window.localStorage.setItem('jwt', token);
                } else {
                    window.localStorage.removeItem('jwt');                    
                }
            }
        )
    }
}

export default AuthStore;