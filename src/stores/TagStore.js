import axios from 'axios'
import { observable, action } from 'mobx'

export default class TagStore {
    @observable popularTags = [];

    @action async loadPopularTags() {
        return axios.get('/api/tags')
            .then((res) => {
                this.popularTags = res.data.tags
            });
    }

    constructor(rootStore) {
        this.rootStore = rootStore;
    }
}