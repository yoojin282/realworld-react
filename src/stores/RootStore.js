import UserStore from './UserStore';
import TagStore from './TagStore';
import ArticleStore from './ArticleStore';
import AuthStore from './AuthStore';
import EditorStore from './EditorStore';
import CommentStore from './CommentStore';
import ProfileStore from './ProfileStore';

class RootStore {

    constructor() {        
        this.userStore = new UserStore(this);
        this.tagStore = new TagStore(this);
        this.articleStore = new ArticleStore(this);
        this.authStore = new AuthStore(this);
        this.editorStore = new EditorStore(this);
        this.commentStore = new CommentStore(this);
        this.profileStore = new ProfileStore(this);
    }
}

export default RootStore;