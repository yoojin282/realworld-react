import { action, observable } from "mobx";
import api from '../Api'

const LIMIT = 10;

class ArticleStore {
    @observable articleList = [];
    @observable page = 0;
    @observable totalPagesCount = 0;
    @observable predicate = {};
    @observable isLoading = false;
    @observable article;

    @action setPredicate(predicate) {        
        if(JSON.stringify(predicate) === JSON.stringify(this.predicate)) return;        
        this.predicate = predicate;
    }

    @action setPage(page) {
        this.page = page;
    }

    @action async loadArticleList() {
        const apiPath = this.predicate.feed ? '/api/articles/feed' : '/api/articles';
        const params = {
            limit: LIMIT,
            offset: this.page * LIMIT
        };
        if(this.predicate.tag) {
            params.tag = this.predicate.tag;
        } else if(this.predicate.favoritedBy) {
            params.favorited = this.predicate.favoritedBy;
        } else if(this.predicate.author) {
            params.author = this.predicate.author;
        }

        this.isLoading = true;
        this.articleList = [];
        return api.get(apiPath, {params: params})
            .then((res) => {
                this.articleList = res.data.articles;
                this.totalPagesCount = Math.ceil(res.data.articlesCount / LIMIT);
            }).finally(() => {
                this.isLoading = false;
            });
    }

    @action async getArticle(slug) {        
        return this.loadArticle(slug)
            .then(article => this.article = article);
            
    }

    @action async loadArticle(slug) {
        this.isLoading = true;
        return api.get('/api/articles/' + slug)
            .then((res) => {
                return res.data.article;
            }).finally(() => this.isLoading = false);
    }

    @action async updateArticle(article) {        
        return api.put(`/api/articles/${article.slug}`, {article: {
            title: article.title,
            description: article.description,
            body: article.body
        }}).then(res => {
            return res.data.article;
        });
    }

    @action async createArticle(article) {        
        return api.post('/api/articles', {article: article})
            .then(res => {
                return res.data.article;
            });
    }

    @action async deleteArticle(slug) {
        return api.delete(`/api/articles/${slug}`);
    }

    @action async favorite(slug) {
        return api.post(`/api/articles/${slug}/favorite`)
            .then(res => {
                return res.data.article;
            });
    }

    @action async unfavorite(slug) {
        return api.delete(`/api/articles/${slug}/favorite`)
            .then(res => {
                return res.data.article;
            });
    }

    constructor(rootStore) {
        this.rootStore = rootStore;
    }
}

export default ArticleStore;