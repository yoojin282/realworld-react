import { action, observable } from "mobx";

export default class EditorStore {
    @observable slug = undefined;

    @observable title = '';
    @observable description = '';
    @observable body = '';
    @observable tags = [];

    @observable isLoading = false;

    @action loadArticle() {
        if(!this.slug) return Promise.resolve();
        this.isLoading = true;
        return this.rootStore.articleStore.loadArticle(this.slug)
            .then((article) => {
                if(!article) {
                    throw new Error('Cant\'t load original article.');
                }                
                this.title = article.title;
                this.description = article.description;
                this.body = article.body;
                this.tags = article.tagList;
                
            }).finally(() => this.isLoading = false);
    }

    @action setSlug(slug) {
        this.slug = slug;
    }

    @action setTitle(title) {
        this.title = title;
    }

    @action setDescription(descr) {
        this.description = descr;
    }

    @action setBody(body) {
        this.body = body;
    }

    @action reset() {
        this.title = '';
        this.description = '';
        this.body = '';
        this.tags = [];
    }

    @action addTag(tag) {
        if(this.tags.includes(tag)) return;
        this.tags.push(tag);
    }

    @action removeTag(tag) {
        this.tags = this.tags.filter(t => t !== tag);
    }

    @action submit() {
        const article = {
            title: this.title,
            description: this.description,
            body: this.body,
            tagList: this.tags,
            slug: this.slug
        }

        return (this.slug ? this.rootStore.articleStore.updateArticle(article) : this.rootStore.articleStore.createArticle(article));
    }

    constructor(rootStore) {
        this.rootStore = rootStore;
    }
}