import { action, observable } from "mobx";
import api from '../Api'

export default class CommentStore {
    @observable commentList = [];
    @observable slug = undefined;
    @observable isLoading = false;

    @action setSlug(slug) {
        if(this.slug !== slug) {
            this.commentList = [];
            this.slug = slug;
        }
    }

    @action async loadComments() {
        if(!this.slug) return;

        this.isLoading = true;
        return api.get(`/api/articles/${this.slug}/comments`)
            .then(res => {
                this.commentList = res.data.comments;
            }).finally(() => this.isLoading = false);
    }

    @action async submit(body) {
        if(!this.slug) return;
        return api.post(`/api/articles/${this.slug}/comments`, {comment: { body: body }})
            .then(() => {
                this.loadComments();
            });
    }

    @action async delete(commentId) {
        if(!this.slug) return;
        return api.delete(`/api/articles/${this.slug}/comments/${commentId}`)
            .then(() => this.loadComments());
    }

    constructor(rootStore) {
        this.rootStore = rootStore;
    }
}