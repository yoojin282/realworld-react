import api from '../Api'

const { observable, computed, action } = require("mobx");

export default class UserStore {
    @observable currentUser;
    @observable isLoading = false;
    @observable isUpdating = false;

    @computed get isLoggedIn() {
        if(this.currentUser) {
            return true;
        }
        return false;
    }

    @action async updateProfile(user) {        
        this.isUpdating = true;
        return api.put('/api/user', user)
            .then((res) => {
                this.currentUser = res.data.user;
            }).finally(() => {
                this.isUpdating = false;
            });
    }

    @action async loadUser() {        
        this.isLoading = true;
        return api.get('/api/user')
            .then((res) => {
                this.currentUser = res.data.user
            }).finally(() => {
                this.isLoading = false;
            });
    }

    constructor(rootStore) {
        this.rootStore = rootStore;
    }
}