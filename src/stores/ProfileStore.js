import { action, observable } from "mobx";
import api from '../Api'

export default class ProfileStore {
    @observable profile = undefined;

    @action async loadProfile(username) {
        return api.get(`/api/profiles/${username}`)
            .then(res => {
                this.profile = res.data.profile;
            });
    }

    @action async follow(username) {
        return api.post(`/api/profiles/${username}/follow`)
            .then(res => {
                return this.profile = res.data.profile;
            });
    }

    @action async unfollow(username) {
        return api.delete(`/api/profiles/${username}/follow`)
            .then(res => {
                return this.profile = res.data.profile;
            });
    }

    constructor(rootStore) {
        this.rootStore = rootStore;
    }
}