import React from 'react'
import { Link } from 'react-router-dom';

const EmptyPaging = props => {
    return (
        <nav>
            <ul className="pagination">
                <li className="page-item active">
                    <span className="page-link">1</span>
                </li>
            </ul>
        </nav>
    );
};

const PrevBlock = props => {
    const {show, pathname, search, onClick, page} = props;
    if(!show) {
        return null;
    }

    const handleClick = e => {
        e.preventDefault();
        onClick(page);
    };

    return (
        <li className='page-item prev-block-item' onClick={handleClick}>
            <Link to={{pathname: pathname, search: search}} className="page-link">&lt;</Link>
        </li>
    );
}

const NextBlock = props => {
    const {show, pathname, search, onClick, page} = props;
    if(!show) {
        return null;
    }

    const handleClick = e => {
        e.preventDefault();
        onClick(page);
    };

    return (
        <li className='page-item next-block-item' onClick={handleClick}>
            <Link to={{pathname: pathname, search: search}} className="page-link">&gt;</Link>
        </li>
    );
};

const Paging = props => {
    if(!props.show) {
        return null;
    }
    const {totalPageCount, currentPage, pathname} = props;
    if(totalPageCount < 2) {
        return <EmptyPaging />;
    }

    const blockSize = props.blockSize || 10;    
    const blockStartPage = Math.floor(currentPage / blockSize) * blockSize;    
    const blockEndPage = (blockStartPage + blockSize) <= totalPageCount ?  (blockStartPage + blockSize) - 1 : totalPageCount - 1;

    const hasNextBlock = blockEndPage < (totalPageCount - 1);
    const hasPrevBlock = (blockStartPage / blockSize) >= 1;
    const range = [];
    for(let i = blockStartPage; i < blockEndPage + 1; i++) {
        range.push(i);
    }

    const onNextPrevBlockClick = (v) => {
        props.onChange(v);
    }
    var search = props.search || '';
    if(search === '') {
        search = "?";
    } else {
        search += '&'
    }
    return (
        <nav>
            <ul className="pagination">
                <PrevBlock show={hasPrevBlock} page={blockStartPage - 1 }
                    pathname={pathname} search={`${search}&page=${blockStartPage - 1 }`} onClick={onNextPrevBlockClick} />
                {
                    range.map(v => {
                        const isCurrent = v === parseInt(currentPage);
                        const onClick = e => {
                            e.preventDefault();
                            props.onChange(v);
                        };

                        return (
                            <li className={isCurrent ? 'page-item active' : 'page-item'}
                                onClick={onClick} key={v.toString()}>
                                <Link to={{pathname: pathname, search: `${search}page=${v}`}} className="page-link">{v + 1}</Link>
                                {/* <a href="#" className="page-link">{v + 1}</a> */}
                            </li>
                        );
                    })
                }
                <NextBlock show={hasNextBlock} page={blockEndPage + 1}
                    pathname={pathname} search={`${search}&page=${blockEndPage + 1}`} onClick={onNextPrevBlockClick} />
            </ul>
        </nav>
    );
};

export default Paging;