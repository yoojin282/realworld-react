import { inject, observer } from 'mobx-react';
import React from 'react'
import { Link } from 'react-router-dom';

const FAVORITED_CLASS = 'btn btn-sm btn-primary';
const NOT_FAVORITED_CLASS = 'btn btn-sm btn-outline-primary';

@inject('articleStore', 'userStore')
@observer
class ArticlePreview extends React.Component {
    handleFavorite = (e) => {
        e.preventDefault();        
        const {article, articleStore , userStore} = this.props;
        if(!userStore.isLoggedIn) return;
        
        (article.favorited ? articleStore.unfavorite(article.slug) : articleStore.favorite(article.slug))
            .then((updated) => {
                article.favorited = updated.favorited;
                article.favoritesCount = updated.favoritesCount;
            });
    }

    render() {
        const {article} = this.props;
        const favoriteButtonClass = article.favorited ? FAVORITED_CLASS : NOT_FAVORITED_CLASS;
        return (
            <div className="article-preview">
                <div className="article-meta">
                    <Link to={`/profile/${article.author.username}`}><img src={article.author.image}  alt={article.author.username} /></Link>
                    <div className="info">
                        <Link to={`/profile/${article.author.username}`} className="author">{article.author.username}</Link>                        
                        <span className="date">{new Date(article.createdAt).toDateString()}</span>
                    </div>
                    <div className="pull-xs-right">
                        <button className={favoriteButtonClass} onClick={this.handleFavorite}>
                            <i className="ion-heart"></i> {article.favoritesCount}
                        </button>
                    </div>
                </div>
                <Link className="preview-link" to={`/article/${article.slug}`}>
                    <h1>{article.title}</h1>
                    <p>{article.description}</p>
                    <span>Read more...</span>
                </Link>
            </div>
        );
    }
}

export default ArticlePreview;