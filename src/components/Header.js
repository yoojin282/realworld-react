import { inject, observer } from 'mobx-react';
import React from 'react'
import { NavLink } from 'react-router-dom';

const LoggedInView = props => {

    const {currentUser} = props;
    if(!currentUser) {
        return null;
    }

    return (
        <ul className="nav navbar-nav pull-xs-right">
            <li className="nav-item">
                {/* <!-- Add "active" class when you're on that page" --> */}
                <NavLink exact to="/" className="nav-link" activeClassName="active">Home</NavLink>                
            </li>
            <li className="nav-item">                
                <NavLink to="/editor" className="nav-link" activeClassName="active">
                    <i className="ion-compose"></i>&nbsp;New Post
                </NavLink>                
            </li>
            <li className="nav-item">
                <NavLink to="/settings" className="nav-link" activeClassName="active">
                    <i className="ion-gear-a"></i>&nbsp;Settings
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink to={`/profile/${currentUser.username}`} className="nav-link" activeClassName="active">
                    {currentUser.username}
                </NavLink>
            </li>
        </ul>
    );
};

const NotLoggedInView = props => {
    if(props.isLoggedIn) {
        return null;
    }

    return (
        <ul className="nav navbar-nav pull-xs-right">
            <li className="nav-item">
                {/* <!-- Add "active" class when you're on that page" --> */}
                <NavLink exact to="/" className="nav-link" activeClassName="active">Home</NavLink>
            </li>           
            <li className="nav-item">
                <NavLink to="/login" className="nav-link" activeClassName="active">Sign in</NavLink>                
            </li>
            <li className="nav-item">
                <NavLink to="/register" className="nav-link" activeClassName="active">Sign up</NavLink>
            </li>
        </ul>
    );
}

@inject('userStore')
@observer
class Header extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-light">
                <div className="container">
                    <NavLink to="/" className="navbar-brand">conduit</NavLink>                    
                    <LoggedInView currentUser={this.props.userStore.currentUser} />
                    <NotLoggedInView isLoggedIn={this.props.userStore.isLoggedIn} />
                </div>
            </nav>
        );
    }
}

export default Header;