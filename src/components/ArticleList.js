import React from "react";
import ArticlePreview from './ArticlePreview'
import LoadingSpinner from "./LoadingSpinner";

const EmptyArticleListView = props => {    
    return <div className="article-preview">No articles.</div>
};

class ArticleList extends React.Component {    
    
    render() {
        const {isLoading, articles} = this.props;
        if(isLoading) {
            return <LoadingSpinner />;
        }

        if(!isLoading && articles.length === 0) {
            return <EmptyArticleListView />;
        }

        return (
            <div>
                { this.props.articles.map(article => {
                    return <ArticlePreview article={article} key={article.slug} />
                })}
            </div>
        );
    }
}

export default ArticleList;