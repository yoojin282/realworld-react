import { inject, observer } from "mobx-react";
import React from "react";
import { Link, withRouter } from "react-router-dom";

const ErrorView = props => {
    if(!props.errors) {
        return null;
    }

    return (
        <ul className="error-messages">
            { props.errors.map(error => {
                return <li key={error}>{error}</li>;
            })}
        </ul>
    );
}


@withRouter
@inject('authStore')
@observer
class Login extends React.Component {
    componentWillUnmount() {
        this.props.authStore.reset();
    }

    handleSubmitForm = (e) => {       
        e.preventDefault();
        this.props.authStore.login()
            .then(() => {
                this.props.history.replace('/');
            });
    }

    handleEmailChange = (e) => {
        this.props.authStore.setEmail(e.target.value);
    }

    handlePasswordChange = (e) => {
        this.props.authStore.setPassword(e.target.value);
    }

    render() {
        const {loginForm, inProgress, errors} = this.props.authStore;        
        return (
            <div className="auth-page">
                <div className="container page">
                    <div className="row">

                        <div className="col-md-6 offset-md-3 col-xs-12">
                            <h1 className="text-xs-center">Sign In</h1>
                            <p className="text-xs-center">
                                <Link to="/register">Need an account?</Link>                                
                            </p>

                            <ErrorView errors={errors} />

                            <form onSubmit={this.handleSubmitForm}>
                                <fieldset className="form-group">
                                    <input className="form-control form-control-lg" type="text" placeholder="Email"  required
                                        onChange={this.handleEmailChange} value={loginForm.email} />
                                </fieldset>

                                <fieldset className="form-group">
                                    <input className="form-control form-control-lg" type="password" placeholder="Password" required
                                        onChange={this.handlePasswordChange} value={loginForm.password} />
                                </fieldset>
                                <button className="btn btn-lg btn-primary pull-xs-right" type="submit" disabled={inProgress}>Sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;