import { inject, observer } from "mobx-react";
import React from "react";
import { Link, withRouter } from "react-router-dom";

const ErrorView = props => {
    if(!props.errors) {
        return null;
    }

    return (
        <ul className="error-messages">
            { props.errors.map(error => {
                return <li key={error}>{error}</li>;
            })}
        </ul>
    );
}


@withRouter
@inject('authStore')
@observer
class Register extends React.Component {
    componentWillUnmount() {
        this.props.authStore.reset();
    }

    handleSubmitForm = (e) => {       
        e.preventDefault();
        this.props.authStore.regist()
            .then(() => {
                this.props.history.replace('/');
            });
    }

    handleEmailChange = (e) => {
        this.props.authStore.setEmail(e.target.value);
    }

    handleUsernameChange = (e) => {
        this.props.authStore.setUsername(e.target.value);
    }

    handlePasswordChange = (e) => {
        this.props.authStore.setPassword(e.target.value);
    }

    render() {
        const {loginForm, inProgress, errors} = this.props.authStore;        
        return (
            <div className="auth-page">
                <div className="container page">
                    <div className="row">

                        <div className="col-md-6 offset-md-3 col-xs-12">
                            <h1 className="text-xs-center">Sign Up</h1>
                            <p className="text-xs-center">
                                <Link to="/login">Have an account?</Link>                                
                            </p>

                            <ErrorView errors={errors} />

                            <form onSubmit={this.handleSubmitForm}>
                                <fieldset className="form-group">
                                    <input className="form-control form-control-lg" type="text" placeholder="Username"  required
                                        onChange={this.handleUsernameChange} value={loginForm.username} />
                                </fieldset>

                                <fieldset className="form-group">
                                    <input className="form-control form-control-lg" type="text" placeholder="Email"  required
                                        onChange={this.handleEmailChange} value={loginForm.email} />
                                </fieldset>

                                <fieldset className="form-group">
                                    <input className="form-control form-control-lg" type="password" placeholder="Password" required
                                        onChange={this.handlePasswordChange} value={loginForm.password} />
                                </fieldset>
                                <button className="btn btn-lg btn-primary pull-xs-right" type="submit" disabled={inProgress}>Sign up</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;