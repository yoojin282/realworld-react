import { inject, observer } from 'mobx-react';
import React from 'react'
import { NavLink, withRouter } from 'react-router-dom';
import ArticleList from '../components/ArticleList';
import Paging from '../components/Paging';
import { parse as qsParse } from "query-string";

const FOLLOWED_CLASS = 'btn btn-sm btn-outline-secondary action-btn';
const NOT_FOLLOWED_CLASS = 'btn btn-sm btn-secondary action-btn';

const FollowButton = props => {
    const {following, onFollow, username, show} = props;
    if(!show) {
        return null;
    }
    const followButtonClassName = following ? FOLLOWED_CLASS : NOT_FOLLOWED_CLASS;
    return (
        <button className={followButtonClassName} onClick={onFollow}>
            <i className={following ? 'ion-minus-round' : 'ion-plus-round'}></i>&nbsp;{(following ? 'Unfollow ' : 'Follow ') + username}
        </button>
    );
};

@inject('profileStore', 'articleStore', 'userStore')
@withRouter
@observer
class Profile extends React.Component {
   
    componentDidMount() {
        const username = this.props.match.params.username;
        const { profileStore } = this.props;
        profileStore.loadProfile(username);
        this.loadArticles();
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {            
            this.loadArticles();
        }

    }

    loadArticles() {        
        const { articleStore } = this.props;
        articleStore.setPage(qsParse(this.props.location.search).page || 0);
        articleStore.setPredicate(this.getPredicate());
        articleStore.loadArticleList();
    }

    getPredicate() {
        if(this.getTab() === 'favorites') {
            return { favoritedBy: this.props.match.params.username };
        }
        return { author: this.props.match.params.username };
    }

    getTab() {
        if(/\/favorites/.test(this.props.location.pathname)) return 'favorites';
        return 'all'
    }

    handleFollow = () => {
        const {profileStore} = this.props;        
        if(profileStore.profile.following) {
            profileStore.unfollow(profileStore.profile.username);
        } else {
            profileStore.follow(profileStore.profile.username);
        }
    }

    handleChangePage = (page) => {        
        this.loadArticles();
    }

    renderTabs() {
        const profile = this.props.profileStore.profile;
        return (
            <ul className="nav nav-pills outline-active">
                <li className="nav-item">
                    <NavLink className="nav-link" to={`/profile/${profile.username}`} isActive={(match, location) => {
                        return location.pathname.match('/favorites') ? false : true;
                    }}>My Articles</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to={`/profile/${profile.username}/favorites`} isActive={(match, location) => {
                        return location.pathname.match('/favorites') ? true : false;
                    }}>Favorited Articles</NavLink>
                </li>
            </ul>
        );
    }

    render() {
        const { profileStore, articleStore, userStore } = this.props;
        const profile = profileStore.profile;        
        if (!profile) {
            return null;
        }

        const pathname = this.getTab() === 'favorites' ? `/profile/${profile.username}/favorites` : `/profile/${profile.username}`;
        return (
            <div className="profile-page">
                <div className="user-info">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-md-10 offset-md-1">
                                <img src={profile.image} className="user-img" alt={profile.username} />
                                <h4>{profile.username}</h4>
                                <p>
                                    {profile.bio}
                                </p>
                                <FollowButton onFollow={this.handleFollow} username={profile.username} following={profile.following} 
                                    show={userStore.currentUser && (userStore.currentUser.username !== profile.username)} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-md-10 offset-md-1">
                            <div className="articles-toggle">
                                {this.renderTabs()}
                            </div>

                            <ArticleList isLoading={articleStore.isLoading} articles={articleStore.articleList} />
                            <div className="text-xs-center">
                                <Paging show={!articleStore.isLoading} blockSize={10} totalPageCount={articleStore.totalPagesCount} 
                                    pathname={pathname}
                                    onChange={this.handleChangePage} currentPage={articleStore.page} />
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        );
    }
}

export default Profile