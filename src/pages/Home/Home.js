import { inject, observer } from 'mobx-react';
import React from 'react'
import Banner from './Banner';
import MainView from './MainView';
import Tags from './Tags';

@inject('tagStore')
@observer
class Home extends React.Component {
    componentDidMount() {
        this.props.tagStore.loadPopularTags();
    }

    render() {
        return (
            <div className="home-page">
                <Banner />

                <div className="container page">
                    <div className="row">

                        <MainView />

                        <div className="col-md-3">
                            <div className="sidebar">
                                <p>Popular Tags</p>
                                <Tags tags={this.props.tagStore.popularTags} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;