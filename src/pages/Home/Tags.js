import React from 'react'
import { NavLink } from 'react-router-dom';

const Tags = props => {
    return (
        <div className="tag-list">
            { props.tags.map(tag => {
                return (
                    <NavLink to={{ pathname: '/', search: "?tab=tag&tag=" + tag}} className='tag-pill tag-default' key={tag}>{tag}</NavLink>
                );
            })}
        </div>
    );
}

export default Tags;