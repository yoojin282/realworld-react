import { inject, observer } from 'mobx-react';
import React from 'react'
import { NavLink, withRouter } from 'react-router-dom';
import { parse as qsParse } from "query-string";
import ArticleList from '../../components/ArticleList';
import Paging from '../../components/Paging';

const YourFeedTab = props => {
    if (!props.isLoggedIn) {
        return null;
    }

    return (
        <li className="nav-item">
            <NavLink className="nav-link" replace to={{pathname: "/", search: "?tab=feed"}} isActive={(match, location) => {
                return location.search.match(/tab=feed/) ? true : false;
            }}>Your Feed</NavLink>
        </li>
    );
};

const GlobalTab = props => {
    return (
        <li className="nav-item">
            <NavLink className="nav-link" replace to={{pathname: "/", search: "?tab=all"}} isActive={(match, location) => {
                return !location.search.match(/tab=(feed|tag)/) ? true: false;
            }}>Global Feed</NavLink>            
        </li>
    );
};

const TagFilteredTab = props => {
    if(!props.tag) {
        return null;
    }
    
    return (
        <li className="nav-item">
            <div className="nav-link active">
                <i className="ion-pound" /> {props.tag}
            </div>            
        </li>
    );
};

@inject("articleStore", "userStore")
@withRouter
@observer
class MainView extends React.Component {    

    componentDidMount() {
        const {articleStore} = this.props;
        articleStore.setPage(this.getPage());
        articleStore.setPredicate(this.getPredicates());
        articleStore.loadArticleList();
    }

    componentDidUpdate(prevProps) {
        if(this.getTab(this.props) !== this.getTab(prevProps) ||
            this.getTag(this.props) !== this.getTag(prevProps) || 
            this.getPage(this.props) !== this.getPage(prevProps)) {
            
            const {articleStore} = this.props;
            articleStore.setPage(this.getPage());
            articleStore.setPredicate(this.getPredicates());
            articleStore.loadArticleList();
        }
        
    }

    getPage(props = this.props) {
        return parseInt(qsParse(props.location.search).page) || 0;
    }

    getTag(props = this.props) {
        return qsParse(props.location.search).tag || '';
    }

    getSearchTag() {
        const tag = this.getTag();
        if(tag === '') {
            return '';
        }
        return `&tag=${tag}`;
    }
    
    getTab(props = this.props) {        
        return qsParse(props.location.search).tab || "all";
    }

    getPredicates(props = this.props) {
        switch (this.getTab(props)) {
            case "feed":                
                return {feed: true};
            case "tag":
                return {tag: qsParse(props.location.search).tag};
            default:
                return {};
        }
    }

    handleChangePage = (page) => {
        const {articleStore} = this.props;
        articleStore.setPage(page);
        articleStore.loadArticleList();
    }

    // handleTabchange = tab => {
    //     console.log('handle tab change');
    // }

    render() {
        const searchTag = this.getSearchTag();
        const currentTab = this.getTab();
        const {userStore, articleStore, location} = this.props;
        return (
            <div className="col-md-9">
                <div className="feed-toggle">
                    <ul className="nav nav-pills outline-active">
                        <YourFeedTab isLoggedIn={userStore.isLoggedIn} tab={this.getTab} />
                        <GlobalTab tab={this.getTab} />
                        <TagFilteredTab tag={qsParse(location.search).tag} />
                    </ul>
                </div>                
                <ArticleList isLoading={articleStore.isLoading} articles={articleStore.articleList} />
                <div className="text-xs-center">
                    <Paging show={!articleStore.isLoading} blockSize={10} totalPageCount={articleStore.totalPagesCount} 
                        pathname={"/"} search={`?tab=${currentTab}${searchTag}`}
                        onChange={this.handleChangePage} currentPage={articleStore.page} />
                </div>
            </div>
        );
    }
}

export default MainView;