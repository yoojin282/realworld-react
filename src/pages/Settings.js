import { inject, observer } from 'mobx-react';
import React from 'react';
import { withRouter } from 'react-router-dom';
import LoadingSpinner from '../components/LoadingSpinner';

class SettingsForm extends React.Component {
    constructor() {
        super();
        this.state = {
            image: '',
            username: '',
            bio: '',
            email: '',
            password: ''
        };

        this.updateState = field => e => {
            const state = this.state;
            const newState = Object.assign({}, state, {[field]: e.target.value});
            this.setState(newState);
        };

        this.submitForm = e => {
            e.preventDefault();
            const user = Object.assign({}, this.state);
            if(!user.password) {
                delete user.password;
            }

            this.props.onSubmitForm(user);
        };
    }

    componentDidMount() {        
        const {currentUser} = this.props;
        if(currentUser) {            
            const currentState = Object.assign({}, {
                image: currentUser.image || '',
                username: currentUser.username,
                bio: currentUser.bio || '',
                email: currentUser.email,
            });

            this.setState(currentState);
        }
    }

    componentDidUpdate(prevProps) {        
        const {currentUser} = this.props;
        if(currentUser && !prevProps.currentUser) {            
            const currentState = Object.assign({}, {
                image: currentUser.image || '',
                username: currentUser.username,
                bio: currentUser.bio || '',
                email: currentUser.email,
            });

            this.setState(currentState);
        }
    }
    
    render() {
        if(this.props.isLoading) {
            return <LoadingSpinner />
        }
        return (
            <form onSubmit={this.submitForm}>
                <fieldset>
                    <fieldset className="form-group">
                        <input className="form-control" type="text" placeholder="URL of profile picture" 
                            value={this.state.image} onChange={this.updateState('image')} />
                    </fieldset>
                    <fieldset className="form-group">
                        <input className="form-control form-control-lg" type="text" placeholder="Your Name" 
                            value={this.state.username} onChange={this.updateState('username')} />
                    </fieldset>
                    <fieldset className="form-group">
                        <textarea className="form-control form-control-lg" rows="8" placeholder="Short bio about you"
                            value={this.state.bio} onChange={this.updateState('bio')}></textarea>
                    </fieldset>
                    <fieldset className="form-group">
                        <input className="form-control form-control-lg" type="text" placeholder="Email"
                            value={this.state.email} onChange={this.updateState('email')} />
                    </fieldset>
                    <fieldset className="form-group">
                        <input className="form-control form-control-lg" type="password" placeholder="Password"
                            value={this.state.password} onChange={this.updateState('password')} />
                    </fieldset>
                    <button className="btn btn-lg btn-primary pull-xs-right" type="submit" disabled={this.props.isUpdating}>Update Settings</button>
                </fieldset>
            </form>
        );
    }
}


@inject('authStore', 'userStore')
@withRouter
@observer
class Settings extends React.Component {
    handleLogout = (e) => {
        e.preventDefault();
        this.props.authStore.logout().then(() => {
            this.props.history.replace("/");
        });
    }

    render() {        
        const {userStore} = this.props;
        return (
            <div className="settings-page">
                <div className="container page">
                    <div className="row">
                        <div className="col-md-6 offset-md-3 col-xs-12">
                            <h1 className="text-xs-center">Your Settings</h1>
                            <SettingsForm onSubmitForm={user => userStore.updateProfile(user)} isLoading={userStore.isLoading}
                                currentUser={this.props.userStore.currentUser} isUpdating={userStore.isUpdating} />
                            <hr />
                            <button className="btn btn-outline-danger" onClick={this.handleLogout}>Or click here to logout.</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Settings