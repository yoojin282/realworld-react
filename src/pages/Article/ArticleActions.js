import { observer } from 'mobx-react';
import React from 'react'
import { Link } from 'react-router-dom';

const FAVORITED_CLASS = "btn btn-sm btn-primary";
const UNFAVORITED_CLASS = "btn btn-sm btn-outline-primary";
const FOLLOWED_CLASS = "btn btn-sm btn-outline-secondary";
const UNFOLLOWED_CLASS = "btn btn-secondary btn-sm";

const ArticleActions = observer(props => {
    const { article, isOwner, onDelete, onFollow, onFavorite } = props;

    const handleDelete = (e) => {
        e.preventDefault();
        onDelete(article.slug);
    };

    const handleFavorite = (e) => {
        e.preventDefault();
        onFavorite(article.slug);
    };

    const handleFollow = (e) => {
        e.preventDefault();
        onFollow(article.author.username);
    };

    if(isOwner) {
        return (
            <span>
                <Link className="btn btn-outline-secondary btn-sm" to={`/editor/${article.slug}`}><i className="ion-edit"></i> Edit Article</Link>
                &nbsp;&nbsp;
                <button className="btn btn-outline-danger btn-sm" onClick={handleDelete}><i className="ion-trash-a"></i> Delete Article</button>                
            </span>
        );
    }

    const favoritedClass = article.favorited ? FAVORITED_CLASS : UNFAVORITED_CLASS;
    const followedClass = article.author.following ? UNFOLLOWED_CLASS : FOLLOWED_CLASS;
    return (
        <span>
            <button className={followedClass} onClick={handleFollow}>
                <i className="ion-plus-round"></i>&nbsp;{article.author.username}
            </button>
            &nbsp;&nbsp;
            <button className={favoritedClass} onClick={handleFavorite}>
                <i className="ion-heart"></i>&nbsp;Favorite Post <span className="counter">({article.favoritesCount})</span>
            </button>
        </span>
    );
});

export default ArticleActions;