import { inject, observer } from 'mobx-react';
import React from 'react'
import { withRouter } from 'react-router-dom';
import ArticleMeta from './ArticleMeta';
import CommentContainer from './CommentContainer';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import LoadingSpinner from '../../components/LoadingSpinner';
import ReactMarkdown from 'react-markdown';

@inject('articleStore', 'userStore', 'commentStore', 'profileStore')
@withRouter
@observer
class Article extends React.Component {
    
    componentDidMount() {        
        const {articleStore, commentStore} = this.props;
        const slug = this.props.match.params.slug;
        articleStore.getArticle(slug).then(() => {
            commentStore.setSlug(slug);
            commentStore.loadComments();
        });
    }

    handleCommentDelete = commentId => {
        confirmAlert({
            title: '주의',
            message: '해당 댓글을 삭제하시겠습니까?',
            buttons: [
                {
                    label: '삭제',
                    onClick: () => {
                        this.props.commentStore.delete(commentId);
                    }
                },
                {
                    label: '취소',
                    onClick: () => {
                        console.log('취소');
                    }
                }
            ]
        });

    }

    handleDelete = slug => {        
        const {articleStore, history} = this.props;
        if(articleStore.article.slug !== slug) {
            return;
        }

        confirmAlert({
            title: '주의',
            message: '해당 포스트를 삭제하시겠습니까?',
            buttons: [
                {
                    label: '삭제',
                    onClick: () => {
                        articleStore.deleteArticle(slug).then(() => {
                            history.replace('/');
                        });
                    }
                },
                {
                    label: '취소',
                    onClick: () => {
                        console.log('취소');
                    }
                }
            ]
        });
    }

    handleFollow = username => {        
        
        const {userStore, articleStore} = this.props;
        if(userStore.currentUser && (username === userStore.currentUser.username)) {
            return;
        }

        const {profileStore} = this.props;
        const {article} = articleStore;
        (article.author.following ? profileStore.unfollow(username) : profileStore.follow(username))
            .then((updated) => {
                article.author.following = updated.following;
            });
    }

    handleFavorite = slug => {        
        const {articleStore} = this.props;
        const article = articleStore.article;
        if(articleStore.article.slug !== slug) {
            return;
        }

        (article.favorited ? articleStore.unfavorite(slug) : articleStore.favorite(slug))
            .then((updated) => {
                article.favorited = updated.favorited;
                article.favoritesCount = updated.favoritesCount;
            });
    }

    render() {
        const {articleStore, userStore, commentStore} = this.props;
        const article = articleStore.article;
        if(articleStore.isLoading) {
            return (<LoadingSpinner />);
        }
        
        if(!article) return null;
        const isOwner = userStore.isLoggedIn && (article.author.username === userStore.currentUser.username);

        return (
            <div className="article-page">
                <div className="banner">
                    <div className="container">
                        <h1>{article.title}</h1>
                        <ArticleMeta article={article} isOwner={isOwner} 
                            onDelete={this.handleDelete} onFollow={this.handleFollow} onFavorite={this.handleFavorite} />
                    </div>
                </div>

                <div className="container page">
                    <div className="row article-content">
                        <div className="col-md-12">
                            <ReactMarkdown source={article.body} />
                        </div>
                    </div>
                    <hr />
                    
                    <div className="article-actions">
                        <ArticleMeta article={article} isOwner={isOwner} 
                            onDelete={this.handleDelete} onFollow={this.handleFollow} onFavorite={this.handleFavorite} />
                    </div>

                    <div className="row">
                        <CommentContainer isLoading={commentStore.isLoading} currentUser={userStore.currentUser} commentList={commentStore.commentList} onCommentDelete={this.handleCommentDelete} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Article