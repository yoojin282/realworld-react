import React from 'react'
import { Link } from 'react-router-dom';
import ArticleActions from './ArticleActions';

const ArticleMeta = props => {
    const {article, isOwner, onDelete, onFollow, onFavorite} = props;
    if(!article) {
        return null;
    }
    return (
        <div className="article-meta">
            <Link to={`/profile/${article.author.username}`}><img src={article.author.image} alt={article.author.username} /></Link>
            
            <div className="info">
                <Link to={`/profile/${article.author.username}`}>{article.author.username}</Link>                
                <span className="date">{new Date(article.createdAt).toDateString()}</span>
            </div>
            <ArticleActions article={article} isOwner={isOwner} onDelete={onDelete} onFavorite={onFavorite} onFollow={onFollow} />
        </div>
    );
};

export default ArticleMeta;