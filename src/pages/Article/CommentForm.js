import { inject } from 'mobx-react';
import React from 'react'

@inject('commentStore')
class CommentForm extends React.Component {
    constructor() {
        super();
        this.state = {
            body: '',
            isSubmit: false
        };

        this.handleChangeBody = e => {
            this.setState({body: e.target.value});
        };

        this.handleSubmitForm = e => {
            e.preventDefault();
            this.setState({isSubmit: true});
            this.props.commentStore.submit(this.state.body.trim()).then(() => this.setState({body: ''}))
                .finally(() => this.setState({isSubmit: false}));
        }
    }
    
    render() {
        return (
            <form className="card comment-form" onSubmit={this.handleSubmitForm}>
                <div className="card-block">
                    <textarea className="form-control" placeholder="Write a comment..." rows="3" value={this.state.body} onChange={this.handleChangeBody}></textarea>
                </div>
                <div className="card-footer">
                    <img src={this.props.currentUser.image} className="comment-author-img" alt={this.props.currentUser.username} />
                    <button className="btn btn-sm btn-primary" type="submit" disabled={this.state.isSubmit}>Post Comment</button>
                </div>
            </form>
        );
    }
}

export default CommentForm