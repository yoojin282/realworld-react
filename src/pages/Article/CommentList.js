import { observer } from 'mobx-react'
import React from 'react'
import LoadingSpinner from '../../components/LoadingSpinner';
import Comment from './Comment'

const CommentList = observer(props => {
    const {commentList, currentUser, isLoading} = props;
    if(isLoading) {
        return <LoadingSpinner />;
    }
    
    return (
        <div>
            { 
                commentList.map( comment => {
                    return <Comment key={comment.id} comment={comment} onDelete={props.onCommentDelete}
                        isOwner={currentUser && (comment.author.username === currentUser.username)}  />
                }) 
            }
            
        </div>
    );
});

export default CommentList;