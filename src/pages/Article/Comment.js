import React from 'react'
import { Link } from 'react-router-dom';

const OwnerActions = props => {
    if(!props.show) {
        return null;
    }

    const handleRemove = () => props.onDelete(props.commentId);

    return (
        <span className="mod-options">
            <i className="ion-edit"></i>
            &nbsp;&nbsp;
            <i className="ion-trash-a" onClick={handleRemove}></i>
        </span>
    );
};

const Comment = props => {
    const {comment, isOwner } = props;
    return (
        <div className="card">
            <div className="card-block">
                <p className="card-text">{comment.body}</p>
            </div>
            <div className="card-footer">
                <Link to={`/profile/${comment.author.username}`} className="comment-author">
                    <img src={comment.author.image} className="comment-author-img" alt={comment.author.username} />
                </Link>
                &nbsp;
                <Link to={`/profile/${comment.author.username}`} className="comment-author">{comment.author.username}</Link>
                <span className="date-posted">Dec 29th</span>

                <OwnerActions commentId={comment.id} show={isOwner} onDelete={props.onDelete} />
            </div>
        </div>
    );
};

export default Comment;