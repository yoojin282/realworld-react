import React from 'react' 
import CommentForm from './CommentForm';
import CommentList from './CommentList';

const CommentContainer = props => {
    if(!props.currentUser) {
        return (
            <div className="col-xs-12 col-md-8 offset-md-2">                
                <CommentList isLoading={props.isLoading} commentList={props.commentList} />
            </div>
        );
    }
    return (
        <div className="col-xs-12 col-md-8 offset-md-2">
            <div>
                <CommentForm currentUser={props.currentUser} />
            </div>

            <CommentList isLoading={props.isLoading} currentUser={props.currentUser} commentList={props.commentList} onCommentDelete={props.onCommentDelete} />
        </div>
    );
}

export default CommentContainer;