import { inject, observer } from 'mobx-react';
import React from 'react'
import { withRouter } from 'react-router-dom';
import LoadingSpinner from '../components/LoadingSpinner';

@inject('editorStore')
@withRouter
@observer
class Editor extends React.Component {
    state = {
        tag: "",
        isSubmit: false
    };

    componentDidMount() {        
        this.loadArticle();
    }

    componentDidUpdate(prevProps) {
        if(this.props.match.params.slug !== prevProps.match.params.slug) {
            this.loadArticle();
        }
    }

    loadArticle = () => {
        const {editorStore} = this.props;
        editorStore.setSlug(this.props.match.params.slug);
        editorStore.loadArticle();
    }

    handleChangeTag = e => this.setState({tag: e.target.value});
    handleChangeTitle = e => this.props.editorStore.setTitle(e.target.value);
    handleChangeDescr = e => this.props.editorStore.setDescription(e.target.value);
    handleChangeBody = e => this.props.editorStore.setBody(e.target.value);
    handleKeyDownTag = e => {
        switch(e.keyCode) {
            case 13: // enter
            case 9:  // tab
            case 188: // ,
                if(e.keyCode !== 9) e.preventDefault();
                this.handleAddTag();
                break;
            default:
                break;
        }
    };

    handleAddTag = () => {
        if(this.state.tag) {
            this.props.editorStore.addTag(this.state.tag.trim());
            this.setState({ tag: ''});
        }
    }

    handleRemoveTag = (tag) => {
        this.props.editorStore.removeTag(tag);
    }

    handleSubmitForm = (e) => {
        e.preventDefault();
        const {editorStore} = this.props;
        this.setState({isSubmit: true});
        editorStore.submit().then(article => {
            editorStore.reset();
            this.props.history.replace(`/article/${article.slug}`);
        });
    }


    render() {        
        const { editorStore } = this.props;
        if(editorStore.isLoading) {
            return <LoadingSpinner />
        }
        return (
            <div className="editor-page">
                <div className="container page">
                    <div className="row">
                        <div className="col-md-10 offset-md-1 col-xs-12">
                            <form onSubmit={this.handleSubmitForm}>
                                <fieldset>
                                    <fieldset className="form-group">
                                        <input type="text" className="form-control form-control-lg" placeholder="Article Title"
                                            value={editorStore.title} onChange={this.handleChangeTitle} />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <input type="text" className="form-control" placeholder="What's this article about?"
                                            value={editorStore.description} onChange={this.handleChangeDescr} />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <textarea className="form-control" rows="8" placeholder="Write your article (in markdown)"
                                            value={editorStore.body} onChange={this.handleChangeBody}></textarea>
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <input type="text" className="form-control" placeholder="Enter tags" 
                                            onChange={this.handleChangeTag} onKeyDown={this.handleKeyDownTag} 
                                            onBlur={this.handleAddTag} value={this.state.tag} />
                                        <div className="tag-list">
                                            {editorStore.tags.map(tag => {
                                                return (
                                                    <span className="tag-default tag-pill" key={tag}>
                                                        <i className="ion-close-round" onClick={() => this.handleRemoveTag(tag)} /> {tag}
                                                    </span>
                                                );
                                            })}
                                        </div>
                                    </fieldset>
                                    <button className="btn btn-lg pull-xs-right btn-primary" type="submit" disabled={this.state.isSubmit}>
                                        Publish Article
                                    </button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Editor;