import axios from 'axios'

let instance = axios.create({
    timeout: 5000
})

// Add a request interceptor
instance.interceptors.request.use((config) => {
    const token = localStorage.getItem('jwt');
    config.headers.Authorization = token ? `Token ${token}` : '';    
    return config;
});

instance.interceptors.response.use(res => {
    return res;
}, error => {
    console.log(error);
    if(error.status === 401) {
        localStorage.removeItem('jwt');
    }
    return Promise.reject(error);
});

export default instance;